#include <SDL_mixer.h>
#include <string>

class AppAudio
{
private:
    Mix_Music *music = NULL;
    Mix_Chunk *scratch = NULL;
    Mix_Chunk *high = NULL;
    Mix_Chunk *medium = NULL;
    Mix_Chunk *low = NULL;
public:
    int StartAudio();
    bool LoadMusic(std::string p_path);
    bool LoadMedium(std::string p_path);
    int PlayMedium();
    int PlayMusic();
    AppAudio();
    ~AppAudio();
};