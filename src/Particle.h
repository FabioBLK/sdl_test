#pragma once
#include "AppVideo.h"

const int TOTAL_PARTICLES = 25;

class Particle
{
public:
	Particle(int x, int y, AppVideo* p_video, SDL_Texture* p_texture);
	void Render();
	bool IsDead();
	~Particle();

private:
	int posX;
	int posY;
	int frame;
	AppVideo* appVideo;
	SDL_Texture* texture;
};