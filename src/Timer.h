#pragma once
#include <SDL.h>

class Timer
{
private:
	Uint32 startTicks;
	Uint32 pauseTicks;
	bool isPaused;
	bool isStarted;
public:
	Timer();
	~Timer();
	void Start();
	void Stop();
	void Pause();
	void Unpause();
	bool IsStarted();
	bool IsPaused();
	Uint32 GetTicks();
};