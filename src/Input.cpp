#include "Input.h"

enum KeyPressSurfaces
{
	KEY_PRESS_SURFACE_DEFAULT,
	KEY_PRESS_SURFACE_UP,
	KEY_PRESS_SURFACE_DOWN,
	KEY_PRESS_SURFACE_LEFT,
	KEY_PRESS_SURFACE_RIGHT,
	KEY_PRESS_SURFACE_TOTAL
};

Input::Input()
{
	if (SDL_NumJoysticks() < 1)
	{
		SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "No joystick found");
	}
	else
	{
		joystick = SDL_JoystickOpen(0);
		if (joystick == NULL)
		{
			SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Could not connect to joystick");
		}
		else
		{
			joyHaptic = SDL_HapticOpenFromJoystick(joystick);
			if (joyHaptic == NULL)
			{
				SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Could not open Rumble on joystick");
			}
			else
			{
				if (SDL_HapticRumbleInit(joyHaptic) < 0)
				{
					SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Could not open Initializa Rumble on joystick");
				}
			}
		}
	}
}

bool Input::GetKey(Uint8 p_key)
{
	if (keyCode[p_key])
	{
		return true;
	}

	return false;
}

bool Input::GetQuitEvent()
{
	return quit;
}

void Input::CheckEvents()
{
	while (SDL_PollEvent(&event) != 0)
	{
		quit = false;
		mouseButtonDown = false;
		mouseButtonUp = false;
		if (event.type == SDL_QUIT)
		{
			quit = true;
		}
		else if (event.type == SDL_MOUSEMOTION)
		{
			SDL_GetMouseState(&mouseX, &mouseY);
		}
		else if (event.type == SDL_MOUSEBUTTONDOWN)
		{
			mouseButtonDown = true;
		}
		else if (event.type == SDL_MOUSEBUTTONUP)
		{
			mouseButtonUp = true;
		}
		else if (event.type == SDL_JOYAXISMOTION)
		{
			if (event.jaxis.which == 0)
			{
				if (event.jaxis.axis == 0)
				{
					if (event.jaxis.value < -JOYSTICK_DEAD_ZONE)
					{
						xAxis = -1;
					}
					else if (event.jaxis.value > JOYSTICK_DEAD_ZONE)
					{
						xAxis = 1;
					}
					else
					{
						xAxis = 0;
					}
				}
				else if (event.jaxis.axis == 1)
				{
					if (event.jaxis.value < -JOYSTICK_DEAD_ZONE)
					{
						yAxis = -1;
					}
					else if (event.jaxis.value > JOYSTICK_DEAD_ZONE)
					{
						yAxis = 1;
					}
					else
					{
						yAxis = 0;
					}
				}
			}
		}

		keyCode = SDL_GetKeyboardState(NULL);
	}
}

MousePosition Input::GetMousePosition()
{
	currentMousePosition.x = mouseX;
	currentMousePosition.y = mouseY;

	return currentMousePosition;
}

JoystickAxis Input::GetJoystickAxis()
{
	currentJoystickAxis.x = xAxis;
	currentJoystickAxis.y = yAxis;

	return currentJoystickAxis;
}

bool Input::GetMouseButtonDown()
{
	return mouseButtonDown;
}

int Input::RumbleJoystick(double p_strength, int p_time)
{
	if (SDL_HapticRumblePlay(joyHaptic, p_strength, p_time))
	{
		return 1;
	}
	return 0;
}

Input::~Input()
{
}
