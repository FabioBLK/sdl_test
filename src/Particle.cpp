#include "Particle.h"

Particle::Particle(int x, int y, AppVideo* p_video, SDL_Texture* p_texture)
{
	posX = x - 5 + (rand() % 25);
	posY = y - 5 + (rand() % 25);

	frame = rand() % 5;

	appVideo = p_video;
	texture = p_texture;
}

Particle::~Particle()
{
}

void Particle::Render()
{
	if (frame % 2 == 0)
	{
		appVideo->DisplayTextureBlend(texture, 125, posX, posY, 25, 25);
	}
	else
	{
		appVideo->DisplayTexture(texture, posX, posY, 25, 25);
	}
	
	frame++;
}

bool Particle::IsDead()
{
	return frame > 10;
}