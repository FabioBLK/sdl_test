#include "AppAudio.h"
#include "AppVideo.h"
#include "Input.h"
#include "Timer.h"

class GameRun
{
public:
	GameRun();
	~GameRun();
	int GeneralTest01();
	int MovementTest();
	int MovementCircleTest();
	int TilesMovement();
	int InputTest();
	int Start();
	int SoundRecordTest();
private:
	void CapGameLoop(Uint32 p_currentFrameTicks);
	AppVideo video;
	AppAudio audio;
	Input input;
	Timer timer;
	Timer capFPS;
};