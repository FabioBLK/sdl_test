#include "Tile.h"
#include "TileManager.h"

Tile::Tile(int x, int y, int tileType, AppVideo* p_video)
{
	box.x = x;
	box.y = y;
	box.w = TILE_WIDTH;
	box.h = TILE_HEIGHT;

	type = tileType;
	video = p_video;
}

Tile::~Tile()
{

}

void Tile::Render(SDL_Rect& p_camera, SDL_Texture* p_texture, SDL_Rect* p_tileClips)
{
	if (TileManager::CheckColision(p_camera, box))
	{
		video->DisplayTextureClip(p_texture, &p_tileClips[type], box.x - p_camera.x, box.y - p_camera.y, TILE_WIDTH, TILE_HEIGHT);
	}
}

int Tile::GetType()
{
	return type;
}

SDL_Rect Tile::GetBox()
{
	return box;
}