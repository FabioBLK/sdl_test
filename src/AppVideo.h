#pragma once
#include <SDL.h>
#include <string>
#include "FontCache/SDL_FontCache.h"

static int DISPLAY_WIDTH = 1280;
static int DISPLAY_HEIGTH = 720;
const static int MAX_FONT_SIZE = 42;
const static std::string WINDOW_NAME = "SDL Test";
const static std::string TTF_DEFAULT_PATH = "res/font/droidsans.ttf";
const static SDL_Color SDL_COLOR_BLACK = {255, 255, 255, 255};

const int SCREEN_FPS = 60;
const int SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_FPS;

class AppVideo
{
private:
    SDL_Window *window;
    SDL_Renderer *renderer;
    FC_Font *ttfDefault;

public:
    AppVideo();
    ~AppVideo();
    void EndVideo();
    int StartVideo();
    void BeginFrame();
    void EndFrame();
    SDL_Texture *LoadTexture(std::string p_path);
    void DisplayTexture(SDL_Texture *p_texture, int p_xPos, int p_yPos, int p_width, int p_height);
    void DisplayTextureBlend(SDL_Texture *p_texture, Uint8 p_alpha, int p_xPos, int p_yPos, int p_width, int p_height);
    void DisplayTextureClip(SDL_Texture *p_texture, SDL_Rect *p_rectClip, int p_xPos, int p_yPos, int p_width, int p_height);
    void DisplayTextureClipEx(SDL_Texture *p_texture, SDL_Rect *p_rectClip, int p_xPos, int p_yPos, int p_width, int p_height, double p_angle, SDL_Point *p_center, SDL_RendererFlip p_flip);
    void DrawBGRectangle(int p_xPos, int p_yPos, int p_width, int p_height, SDL_Color p_color);
    void DrawText(const std::string &p_text, const int p_xPosition, const int p_yPosition, const int p_size);
};