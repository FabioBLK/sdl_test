#include "DotSprite.h"
#include "TileManager.h"

DotSprite::DotSprite(int p_x, int p_y, Input* p_input, AppVideo* p_video, const std::string p_spritePath, int clipIndex)
{
	posX = p_x;
	posY = p_y;
	velX = 0;
	velY = 0;
	circleCollider.r = DOTSPRITE_WIDTH / 2;

	colliders.resize(11);

	colliders[0].w = 6;
	colliders[0].h = 1;

	colliders[1].w = 10;
	colliders[1].h = 1;

	colliders[2].w = 14;
	colliders[2].h = 1;

	colliders[3].w = 16;
	colliders[3].h = 2;

	colliders[4].w = 18;
	colliders[4].h = 2;

	colliders[5].w = 20;
	colliders[5].h = 6;

	colliders[6].w = 18;
	colliders[6].h = 2;

	colliders[7].w = 16;
	colliders[7].h = 2;

	colliders[8].w = 14;
	colliders[8].h = 1;

	colliders[9].w = 10;
	colliders[9].h = 1;

	colliders[10].w = 6;
	colliders[10].h = 1;

	ShiftColliders();

	input = p_input;
	video = p_video;

	if (clipIndex == 0)
	{
		clip = { 0,0,50,50 };
	}
	else
	{
		clip = { 50,0,50,50 };
	}

	texture = video->LoadTexture(p_spritePath);

	for (int i = 0; i < TOTAL_PARTICLES; ++i)
	{
		particles[i] = new Particle(posX, posY, video, texture);
	}
}

DotSprite::~DotSprite()
{
}

void DotSprite::HandleInput()
{
	velX = 0;
	velY = 0;

	if (input->GetKey(SDL_SCANCODE_UP))
	{
		velY = -DOTSPRITE_VEL;
	}
	if (input->GetKey(SDL_SCANCODE_DOWN))
	{
		velY = DOTSPRITE_VEL;
	}
	if (input->GetKey(SDL_SCANCODE_RIGHT))
	{
		velX = DOTSPRITE_VEL;
	}
	if (input->GetKey(SDL_SCANCODE_LEFT))
	{
		velX = -DOTSPRITE_VEL;
	}
}

void DotSprite::MoveVector(std::vector<SDL_Rect>& otherColliders)
{
	posX += velX;
	ShiftColliders();

	if ((posX < 0) || (posX + DOTSPRITE_WIDTH > DISPLAY_WIDTH) || CheckCollision(colliders, otherColliders))
	{
		posX -= velX;
		ShiftColliders();
	}

	posY += velY;
	ShiftColliders();

	if ((posY < 0) || (posY + DOTSPRITE_HEIGHT > DISPLAY_HEIGTH) || CheckCollision(colliders, otherColliders))
	{
		posY -= velY;
		ShiftColliders();
	}
}

void DotSprite::Move(SDL_Rect& p_square, DotCircle& p_circle, int p_levelWidth, int p_levelHeight)
{
	posX += velX;
	ShiftColliders();

	if ((posX - circleCollider.r < 0) || (posX + circleCollider.r > p_levelWidth)
		|| CheckCollision(circleCollider, p_square) 
		|| CheckCollision(circleCollider, p_circle))
	{
		posX -= velX;
		ShiftColliders();
	}

	posY += velY;
	ShiftColliders();

	if ((posY - circleCollider.r < 0) || (posY - circleCollider.r > p_levelHeight)
		|| (CheckCollision(circleCollider, p_square)) 
		|| (CheckCollision(circleCollider, p_circle)))
	{
		posY -= velY;
		ShiftColliders();
	}
}

void DotSprite::MoveWithTiles(Tile* tiles[])
{
	SDL_Rect box = SDL_Rect{ posX, posY, TILE_WIDTH, TILE_HEIGHT };
	posX += velX;

	if ((posX < 0) 
		|| (posX + DOTSPRITE_WIDTH > LEVEL_WIDTH) 
		|| TileManager::TouchesWall(box, tiles)
	)
	{
		posX -= velX;
	}

	posY += velY;

	if ((posY < 0)
		|| (posY + DOTSPRITE_HEIGHT > LEVEL_HEIGHT)
		|| (TileManager::TouchesWall(box, tiles))
		)
	{
		posY -= velY;
	}
}

void DotSprite::Render()
{
	video->DisplayTextureClip(texture, &clip, posX, posY, 50, 50);
	
	// Enable particles example by enabling next line
	//RenderParticles();
}

void DotSprite::Render(SDL_Rect& p_camera)
{
	video->DisplayTextureClip(texture, &clip, posX - p_camera.x, posY - p_camera.y, 50, 50);
}

void DotSprite::RenderCircle(int camX, int camY)
{
	video->DisplayTextureClip(texture, &clip, (posX - circleCollider.r) - camX, (posY - circleCollider.r) - camY, 50, 50);
}

void DotSprite::ShiftColliders()
{
	circleCollider.x = posX;
	circleCollider.y = posY;

	int r = 0;

	for (int set = 0; set < colliders.size(); set++)
	{
		colliders[set].x = posX + (DOTSPRITE_WIDTH - colliders[set].w) / 2;
		colliders[set].y = posY + r;
		r += colliders[set].h;
	}
}

std::vector<SDL_Rect>& DotSprite::GetColliders()
{
	return colliders;
}

DotCircle& DotSprite::GetCircleCollider()
{
	return circleCollider;
}

bool DotSprite::CheckCollision(std::vector<SDL_Rect>& a, std::vector<SDL_Rect>& b)
{
	int leftA, leftB, rightA, rightB, topA, topB, bottomA, bottomB;

	for (int aBox = 0; aBox < a.size(); aBox++)
	{
		leftA = a[aBox].x;
		topA = a[aBox].y;
		rightA = a[aBox].x + a[aBox].w;
		bottomA = a[aBox].y + a[aBox].h;

		for (int bBox = 0; bBox < b.size(); bBox++)
		{
			leftB = b[bBox].x;
			topB = b[bBox].y;
			rightB = b[bBox].x + b[bBox].w;
			bottomB = b[bBox].y + b[bBox].h;

			if (((bottomA <= topB) || (topA >= bottomB) || (rightA <= leftB) || (leftA >= rightB)) == false)
			{
				return true;
			}
		}
	}

	return false;
}

bool DotSprite::CheckCollision(DotCircle& a, DotCircle& b)
{
	int totalRadiusSquared = a.r + b.r;
	totalRadiusSquared = totalRadiusSquared * totalRadiusSquared;

	double d1 = DistanceSquared(a.x, a.y, b.x, b.y);
	//std::string log = std::to_string(d1);
	//log = "DIST SQRT = " + log + " TRSQRT = " + std::to_string(totalRadiusSquared);
	//SDL_Log(log.c_str());

	if (DistanceSquared(a.x, a.y, b.x, b.y) < (totalRadiusSquared))
	{
		return true;
	}

	return false;
}

bool DotSprite::CheckCollision(DotCircle& a, SDL_Rect& b)
{
	int cx, cy;

	if (a.x < b.x)
	{
		cx = b.x;
	}
	else if (a.x > b.x + b.w)
	{
		cx = b.x + b.w;
	}
	else
	{
		cx = a.x;
	}

	if (a.y < b.y)
	{
		cy = b.y;
	}
	else if(a.y > b.y + b.h)
	{
		cy = b.y + b.h;
	}
	else
	{
		cy = a.y;
	}

	if (DistanceSquared(a.x, a.y, cx, cy) < (double)a.r * (double)a.r)
	{
		return true;
	}

	return false;
}

double DotSprite::DistanceSquared(int x1, int y1, int x2, int y2)
{
	int deltaX = x2 - x1;
	int deltaY = y2 - y1;

	double result = (double)deltaX * (double)deltaX + (double)deltaY * (double)deltaY;
	std::string log = std::to_string(result);
	log = "DIST SQRT = " + log;
	//SDL_Log(log.c_str());

	return result;
}

int DotSprite::GetPosX()
{
	return posX;
}

int DotSprite::GetPosY()
{
	return posY;
}

void DotSprite::RenderParticles()
{
	for (int i = 0; i < TOTAL_PARTICLES; ++i)
	{
		if (particles[i]->IsDead())
		{
			delete particles[i];
			particles[i] = new Particle(posX, posY, video, texture);
		}
	}
	for (int i = 0; i < TOTAL_PARTICLES; ++i)
	{
		particles[i]->Render();
	}
}
