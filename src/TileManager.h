#pragma once
#include <SDL.h>
#include "Tile.h"

namespace TileManager
{
	bool LoadMedia(Tile* p_tiles[], AppVideo* p_video, SDL_Rect* gTileClips[]);
	void Close(Tile* p_tiles[]);
	bool CheckColision(SDL_Rect a, SDL_Rect b);
	bool TouchesWall(SDL_Rect box, Tile* p_tiles[]);
	bool SetTiles(Tile* p_tiles[], AppVideo* p_video, SDL_Rect* gTileClips);
}