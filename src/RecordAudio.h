#pragma once
#include <SDL.h>
#include <string>
#include <vector>
#include <functional>

const int MAX_RECORDING_DEVICES = 10;
const int MAX_RECORDING_SECONDS = 5;
const int RECORDING_BUFFER_SECONDS = MAX_RECORDING_SECONDS + 1;

enum RecordingState
{
	SELECTING_DEVICE,
	SELECTED,
	STOPPED,
	RECORDING,
	RECORDED,
	PLAYBACK,
	ERROR
};

void MyAudioRecordingCallback(void* userData, Uint8* stream, int len);
void MyAudioPlaybackCallback(void* userData, Uint8* stream, int len);

class RecordAudio
{
public:
	RecordAudio();
	~RecordAudio();
	bool loadMedia();
	std::vector<std::string>* DevicesNames();
	SDL_AudioSpec* GetReceivedRecordingSpec();
	SDL_AudioSpec* GetReceivedPlaybackSpec();
	int OpenRecordingDevice(int deviceId);
	int OpenPlaybackDevice();
	void SetupRecording();
	void ResetAndStartRecording(int recordingDeviceId);
	void ResetAndStartPlayback(int recordingDeviceId);
	void StopRecording(int recordingDeviceId);
	void AudioPlaybackCallback(Uint8* stream, int len);
	void AudioRecordingCallback(Uint8* stream, int len);
	bool IsBufferFull();

private:
	int recordingDeviceCount = 0;
	SDL_AudioSpec receivedRecordingSpec;
	SDL_AudioSpec receivedPlaybackSpec;

	Uint8* recordingBuffer = NULL;
	Uint32 bufferByteSize = 0;
	Uint32 bufferBytePosition = 0;
	Uint32 bufferByteMaxPosition = 0;

	std::vector<std::string> deviceNames;
};