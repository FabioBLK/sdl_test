#include <SDL.h>
#include "AppAudio.h"

AppAudio::AppAudio()
{
}

int AppAudio::StartAudio()
{
    int result = 0;
    result = Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 2048);
    if (result < 0)
    {
        SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Error Initializing SDL Audio Mixer");
    }

    return result;
}

bool AppAudio::LoadMusic(std::string p_path)
{
    music = Mix_LoadMUS(p_path.c_str());
    if (music == NULL)
    {
        return false;
    }
    return true;
}

bool AppAudio::LoadMedium(std::string p_path)
{
    medium = Mix_LoadWAV(p_path.c_str());
    if (medium == NULL)
    {
        return false;
    }
    return true;
}

int AppAudio::PlayMusic()
{
    if (Mix_PlayingMusic() == 0)
    {
        Mix_PlayMusic(music, 0);
    }
    else
    {
        if (Mix_PausedMusic() == 1)
        {
            Mix_ResumeMusic();
        }
        else
        {
            Mix_PauseMusic();
        }
    }
    return 0;
}

int AppAudio::PlayMedium()
{
    Mix_PlayChannel(-1, medium, 0);
    return 0;
}

AppAudio::~AppAudio()
{
}
