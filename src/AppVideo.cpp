#include "AppVideo.h"
#include <SDL_image.h>

AppVideo::AppVideo(/* args */)
{
}

int AppVideo::StartVideo()
{
    int init = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_HAPTIC | SDL_INIT_AUDIO);
    if (init < 0)
    {
        SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Error Initializing SDL");
        return -1;
    }

    window = SDL_CreateWindow(&WINDOW_NAME[0u], SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, DISPLAY_WIDTH, DISPLAY_HEIGTH, SDL_WINDOW_OPENGL);
    if (!window)
    {
        SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Error Initializing SDL Window");
        return -1;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!renderer)
    {
        SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Error Initializing SDL Renderer");
        return -1;
    }

    ttfDefault = FC_CreateFont();
    FC_LoadFont(ttfDefault, renderer, TTF_DEFAULT_PATH.c_str(), 20, SDL_COLOR_BLACK, TTF_STYLE_NORMAL);

    return 0;
}

void AppVideo::EndVideo()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void AppVideo::BeginFrame()
{
    //SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_GetWindowSize(window, &DISPLAY_WIDTH, &DISPLAY_HEIGTH);

    SDL_Rect frame = {0, 0, DISPLAY_WIDTH, DISPLAY_HEIGTH};
    SDL_RenderFillRect(renderer, &frame);
}

void AppVideo::DrawBGRectangle(int p_xPos, int p_yPos, int p_width, int p_height, SDL_Color p_color)
{
    SDL_SetRenderDrawColor(renderer, p_color.r, p_color.g, p_color.b, p_color.a);
    SDL_Rect rect;
    rect.x = p_xPos;
    rect.y = p_yPos;
    rect.w = p_width;
    rect.h = p_height;
    SDL_RenderFillRect(renderer, &rect);
}

SDL_Texture *AppVideo::LoadTexture(std::string p_path)
{
    int init = IMG_Init(IMG_INIT_PNG);
    return IMG_LoadTexture(renderer, p_path.c_str());
}

void AppVideo::DisplayTexture(SDL_Texture *p_texture, int p_xPos, int p_yPos, int p_width, int p_height)
{
    SDL_Rect rect;
    rect.x = p_xPos;
    rect.y = p_yPos;
    rect.w = p_width;
    rect.h = p_height;
    SDL_RenderCopy(renderer, p_texture, NULL, &rect);
}

void AppVideo::DisplayTextureClip(SDL_Texture *p_texture, SDL_Rect *p_rectClip, int p_xPos, int p_yPos, int p_width, int p_height)
{
    SDL_Rect rect;
    rect.x = p_xPos;
    rect.y = p_yPos;
    rect.w = p_width;
    rect.h = p_height;
    SDL_RenderCopy(renderer, p_texture, p_rectClip, &rect);
}

void AppVideo::DisplayTextureClipEx(SDL_Texture *p_texture, SDL_Rect *p_rectClip, 
int p_xPos, int p_yPos, int p_width, int p_height, 
double p_angle, SDL_Point *p_center, SDL_RendererFlip p_flip)
{
    SDL_Rect rect;
    rect.x = p_xPos;
    rect.y = p_yPos;
    rect.w = p_width;
    rect.h = p_height;
    SDL_RenderCopyEx(renderer, p_texture, p_rectClip, &rect, p_angle, p_center, p_flip);
}

void AppVideo::DisplayTextureBlend(SDL_Texture *p_texture, Uint8 p_alpha, int p_xPos, int p_yPos, int p_width, int p_height)
{
    SDL_Rect rect;
    rect.x = p_xPos;
    rect.y = p_yPos;
    rect.w = p_width;
    rect.h = p_height;
    SDL_SetTextureBlendMode(p_texture, SDL_BLENDMODE_BLEND);
    SDL_SetTextureAlphaMod(p_texture, p_alpha);
    SDL_RenderCopy(renderer, p_texture, NULL, &rect);
}

void AppVideo::EndFrame()
{
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_RenderPresent(renderer);
}

void AppVideo::DrawText(const std::string &p_text, const int p_xPosition, const int p_yPosition, const int p_size)
{
    FC_Draw(ttfDefault, renderer, p_xPosition, p_yPosition, p_text.c_str());
}

AppVideo::~AppVideo()
{
}