#include "Timer.h"

Timer::Timer()
{
}

Timer::~Timer()
{
}

void Timer::Start()
{
	isPaused = false;
	isStarted = true;
	pauseTicks = 0;

	startTicks = SDL_GetTicks();
}

void Timer::Stop()
{
	startTicks = 0;
	pauseTicks = 0;
	isStarted = false;
	isPaused = false;
}

void Timer::Pause()
{
	if (isStarted && !isPaused)
	{
		pauseTicks = SDL_GetTicks() - startTicks;
		isPaused = true;
	}
}

void Timer::Unpause()
{
	if (isStarted && isPaused)
	{
		startTicks = SDL_GetTicks() - pauseTicks;
		pauseTicks = 0;
		isPaused = false;
	}
}

bool Timer::IsStarted()
{
	return isStarted;
}

bool Timer::IsPaused()
{
	return isPaused;
}

Uint32 Timer::GetTicks()
{
	Uint32 result = 0;

	if (isStarted && !isPaused)
	{
		result = SDL_GetTicks() - startTicks;
	}
	else if (isStarted && isPaused)
	{
		result = pauseTicks;
	}

	return result;
}