#include <string>
#include <vector>
#include "Input.h"
#include "AppVideo.h"
#include "Particle.h"
#include "Tile.h"

struct DotCircle
{
	int x;
	int y;
	int r;
};

class DotSprite
{
public:
	static const int DOTSPRITE_WIDTH = 50;
	static const int DOTSPRITE_HEIGHT = 50;
	static const int DOTSPRITE_VEL = 4;

	DotSprite(int p_x, int p_y, Input* p_input, AppVideo* p_video, const std::string p_spritePath, int clipIndex);
	~DotSprite();

	void HandleInput();
	void MoveVector(std::vector<SDL_Rect> &otherColliders);
	void Move(SDL_Rect& p_square, DotCircle& p_circle, int p_levelWidth, int p_levelHeight);
	void MoveWithTiles(Tile* tiles[]);
	void Render();
	void Render(SDL_Rect& camera);
	void RenderCircle(int camX, int camY);
	bool CheckCollision(std::vector<SDL_Rect>& a, std::vector<SDL_Rect>& b);
	bool CheckCollision(DotCircle& a, DotCircle& b);
	bool CheckCollision(DotCircle& a, SDL_Rect& b);
	std::vector<SDL_Rect>& GetColliders();
	DotCircle& GetCircleCollider();
	double DistanceSquared(int x1, int y1, int x2, int y2);
	int GetPosX();
	int GetPosY();

private:
	int posX;
	int posY;
	int velX;
	int velY;

	std::vector<SDL_Rect> colliders;
	SDL_Rect quadCollider;
	DotCircle circleCollider;

	SDL_Rect clip;
	SDL_Texture* texture;
	Input* input;
	AppVideo* video;

	Particle* particles[TOTAL_PARTICLES];

	void ShiftColliders();
	void RenderParticles();
};