#pragma once
#include <SDL.h>

const int JOYSTICK_DEAD_ZONE = 8000;

struct MousePosition
{
	int x;
	int y;
};

struct JoystickAxis
{
	int x;
	int y;
};

class Input
{
private:
	SDL_Event event;
	const Uint8* keyCode;
	bool quit = false;
	bool mouseButtonDown = false;
	bool mouseButtonUp = false;
	int mouseX = 0;
	int mouseY = 0;
	int xAxis = 0;
	int yAxis = 0;
	MousePosition currentMousePosition;
	JoystickAxis currentJoystickAxis;
	SDL_Joystick* joystick;
	SDL_Haptic* joyHaptic;
public:
	Input();
	~Input();
	void CheckEvents();
	bool GetKey(Uint8 p_key);
	bool GetQuitEvent();
	bool GetMouseButtonDown();
	int RumbleJoystick(double p_strength, int p_time);
	MousePosition GetMousePosition();
	JoystickAxis GetJoystickAxis();
};