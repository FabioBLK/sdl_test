#include "TileManager.h"
#include <fstream>

namespace TileManager
{
	bool LoadMedia(Tile* p_tiles[], AppVideo* p_video, SDL_Rect* gTileClips[])
	{
		bool success = true;

		//if (!SetTiles(p_tiles, p_video, gTileClips))
		//{
		//	printf("Failed to load tile set!\n");
		//	success = false;
		//}

		return success;
	}

	void Close(Tile* p_tiles[])
	{
		//Deallocate tiles
		for (int i = 0; i < TOTAL_TILES; ++i)
		{
			if (p_tiles[i] != NULL)
			{
				delete p_tiles[i];
				p_tiles[i] = NULL;
			}
		}
	}

	bool CheckColision(SDL_Rect a, SDL_Rect b)
	{
		//The sides of the rectangles
		int leftA, leftB;
		int rightA, rightB;
		int topA, topB;
		int bottomA, bottomB;

		//Calculate the sides of rect A
		leftA = a.x;
		rightA = a.x + a.w;
		topA = a.y;
		bottomA = a.y + a.h;

		//Calculate the sides of rect B
		leftB = b.x;
		rightB = b.x + b.w;
		topB = b.y;
		bottomB = b.y + b.h;

		//If any of the sides from A are outside of B
		if (bottomA <= topB)
		{
			return false;
		}

		if (topA >= bottomB)
		{
			return false;
		}

		if (rightA <= leftB)
		{
			return false;
		}

		if (leftA >= rightB)
		{
			return false;
		}

		//If none of the sides from A are outside B
		return true;
	}

	bool TouchesWall(SDL_Rect box, Tile* p_tiles[])
	{
		for (int i = 0; i < TOTAL_TILES; i++) {
			if ((p_tiles[i]->GetType() >= TILE_CENTER) && (p_tiles[i]->GetType() <= TILE_TOPLEFT)) {
				if (CheckColision(box, p_tiles[i]->GetBox())) {
					return true;
				}
			}
		}

		return false;
	}

	bool SetTiles(Tile* p_tiles[], AppVideo* p_video, SDL_Rect* gTileClips)
	{
		bool tilesLoaded = true;

		int x = 0;
		int y = 0;

		std::ifstream map("res/map/lazy.txt");
		if (map.fail()) {
			SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Unable to load map file!\n");
			tilesLoaded = false;
		}
		else
		{
			for (int i = 0; i < TOTAL_TILES; i++) {
				int tileType = -1;
				map >> tileType;
				
				if (map.fail()) {
					SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Error loading map: Unexpected end of file!\n");
					tilesLoaded = false;
					break;
				}

				if ((tileType >= 0) && (tileType < TOTAL_TILE_SPRITES)) {
					p_tiles[i] = new Tile(x, y, tileType, p_video);
				}
				else {
					SDL_LogDebug(SDL_LOG_CATEGORY_ERROR, "Error loading map: Invalid tile type at %d!\n", i);
					tilesLoaded = false;
					break;
				}

				x += TILE_WIDTH;

				if (x >= LEVEL_WIDTH) {
					x = 0;
					y += LEVEL_HEIGHT;
				}
			}

			if (tilesLoaded)
			{
				gTileClips[TILE_RED].x = 0;
				gTileClips[TILE_RED].y = 0;
				gTileClips[TILE_RED].w = TILE_WIDTH;
				gTileClips[TILE_RED].h = TILE_HEIGHT;

				gTileClips[TILE_GREEN].x = 0;
				gTileClips[TILE_GREEN].y = 80;
				gTileClips[TILE_GREEN].w = TILE_WIDTH;
				gTileClips[TILE_GREEN].h = TILE_HEIGHT;

				gTileClips[TILE_BLUE].x = 0;
				gTileClips[TILE_BLUE].y = 160;
				gTileClips[TILE_BLUE].w = TILE_WIDTH;
				gTileClips[TILE_BLUE].h = TILE_HEIGHT;

				gTileClips[TILE_TOPLEFT].x = 80;
				gTileClips[TILE_TOPLEFT].y = 0;
				gTileClips[TILE_TOPLEFT].w = TILE_WIDTH;
				gTileClips[TILE_TOPLEFT].h = TILE_HEIGHT;

				gTileClips[TILE_LEFT].x = 80;
				gTileClips[TILE_LEFT].y = 80;
				gTileClips[TILE_LEFT].w = TILE_WIDTH;
				gTileClips[TILE_LEFT].h = TILE_HEIGHT;

				gTileClips[TILE_BOTTOMLEFT].x = 80;
				gTileClips[TILE_BOTTOMLEFT].y = 160;
				gTileClips[TILE_BOTTOMLEFT].w = TILE_WIDTH;
				gTileClips[TILE_BOTTOMLEFT].h = TILE_HEIGHT;

				gTileClips[TILE_TOP].x = 160;
				gTileClips[TILE_TOP].y = 0;
				gTileClips[TILE_TOP].w = TILE_WIDTH;
				gTileClips[TILE_TOP].h = TILE_HEIGHT;

				gTileClips[TILE_CENTER].x = 160;
				gTileClips[TILE_CENTER].y = 80;
				gTileClips[TILE_CENTER].w = TILE_WIDTH;
				gTileClips[TILE_CENTER].h = TILE_HEIGHT;

				gTileClips[TILE_BOTTOM].x = 160;
				gTileClips[TILE_BOTTOM].y = 160;
				gTileClips[TILE_BOTTOM].w = TILE_WIDTH;
				gTileClips[TILE_BOTTOM].h = TILE_HEIGHT;

				gTileClips[TILE_TOPRIGHT].x = 240;
				gTileClips[TILE_TOPRIGHT].y = 0;
				gTileClips[TILE_TOPRIGHT].w = TILE_WIDTH;
				gTileClips[TILE_TOPRIGHT].h = TILE_HEIGHT;

				gTileClips[TILE_RIGHT].x = 240;
				gTileClips[TILE_RIGHT].y = 80;
				gTileClips[TILE_RIGHT].w = TILE_WIDTH;
				gTileClips[TILE_RIGHT].h = TILE_HEIGHT;

				gTileClips[TILE_BOTTOMRIGHT].x = 240;
				gTileClips[TILE_BOTTOMRIGHT].y = 160;
				gTileClips[TILE_BOTTOMRIGHT].w = TILE_WIDTH;
				gTileClips[TILE_BOTTOMRIGHT].h = TILE_HEIGHT;
			}
		}

		map.close();

		return tilesLoaded;
	}
}