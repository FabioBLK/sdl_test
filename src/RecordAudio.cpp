#include "RecordAudio.h"
#include <iostream>

void MyAudioPlaybackCallback(void* userData, Uint8* stream, int len)
{

}

void MyAudioRecordingCallback(void* userData, Uint8* stream, int len)
{

}

RecordAudio::RecordAudio()
{
	bool success = false;
	recordingDeviceCount = SDL_GetNumAudioDevices(SDL_TRUE);
	if (recordingDeviceCount < 1)
	{
		std::cout << "Unable to get an audio capture device. SDL Error: " << SDL_GetError();
	}
	else
	{
		if (recordingDeviceCount > MAX_RECORDING_DEVICES)
		{
			recordingDeviceCount = MAX_RECORDING_DEVICES;
		}

		for (int i = 0; i < recordingDeviceCount; i++)
		{
			deviceNames.push_back(SDL_GetAudioDeviceName(i, SDL_TRUE));
		}

		success = true;
		std::cout << "Audio device success. Count: " << recordingDeviceCount << std::endl;
	}
}

RecordAudio::~RecordAudio()
{
}

bool RecordAudio::loadMedia()
{


	return true;
}

std::vector<std::string>* RecordAudio::DevicesNames()
{
	return &deviceNames;
}

SDL_AudioSpec* RecordAudio::GetReceivedRecordingSpec()
{
	return &receivedRecordingSpec;
}

SDL_AudioSpec* RecordAudio::GetReceivedPlaybackSpec()
{
	return &receivedPlaybackSpec;
}

int RecordAudio::OpenPlaybackDevice()
{
	SDL_AudioSpec desiredPlaybackSpec;
	SDL_zero(desiredPlaybackSpec);
	desiredPlaybackSpec.freq = 44100;
	desiredPlaybackSpec.format = AUDIO_F32;
	desiredPlaybackSpec.channels = 2;
	desiredPlaybackSpec.samples = 4096;
	desiredPlaybackSpec.callback = [](void* userData, Uint8* stream, int len)
	{
		static_cast<RecordAudio*>(userData)->AudioPlaybackCallback(stream, len);
	};
	desiredPlaybackSpec.userdata = this;

	int playbackDeviceId = SDL_OpenAudioDevice(
		NULL,
		SDL_FALSE,
		&desiredPlaybackSpec,
		&receivedPlaybackSpec,
		SDL_AUDIO_ALLOW_FORMAT_CHANGE
	);

	return playbackDeviceId;
}

int RecordAudio::OpenRecordingDevice(int deviceId)
{
	SDL_AudioSpec desiredRecordingSpec;
	SDL_zero(desiredRecordingSpec);
	desiredRecordingSpec.freq = 44100;
	desiredRecordingSpec.format = AUDIO_F32;
	desiredRecordingSpec.channels = 2;
	desiredRecordingSpec.samples = 4096;
	//desiredRecordingSpec.callback = std::bind(&RecordAudio::AudioRecordingCallback, this, std::placeholders::_3);
	desiredRecordingSpec.callback = [](void* userData, Uint8* stream, int len)
	{
		static_cast<RecordAudio*>(userData)->AudioRecordingCallback(stream, len);
	};
	desiredRecordingSpec.userdata = this;

	int recordingDeviceId = SDL_OpenAudioDevice(
		SDL_GetAudioDeviceName(deviceId, SDL_TRUE),
		SDL_TRUE,
		&desiredRecordingSpec,
		&receivedRecordingSpec,
		SDL_AUDIO_ALLOW_FORMAT_CHANGE
	);

	return recordingDeviceId;
}

void RecordAudio::AudioRecordingCallback(Uint8* stream, int len)
{
	std::cout << "recording";
	memcpy(&recordingBuffer[bufferBytePosition], stream, len);
	bufferBytePosition += len;
}

void RecordAudio::AudioPlaybackCallback(Uint8* stream, int len)
{
	std::cout << "playback " << bufferBytePosition;
	memcpy(stream, &recordingBuffer[bufferBytePosition], len);
	bufferBytePosition += len;
}

void RecordAudio::SetupRecording()
{
	int bytesPerSample = receivedRecordingSpec.channels * (SDL_AUDIO_BITSIZE(receivedRecordingSpec.format) / 8);
	int bytesPerSecond = receivedRecordingSpec.freq * bytesPerSample;
	bufferByteSize = RECORDING_BUFFER_SECONDS * bytesPerSecond;
	bufferByteMaxPosition = MAX_RECORDING_SECONDS * bytesPerSecond;
	recordingBuffer = new Uint8[bufferByteSize];
	memset(recordingBuffer, 0, bufferByteSize);
}

void RecordAudio::ResetAndStartRecording(int recordingDeviceId)
{
	bufferBytePosition = 0;
	SDL_PauseAudioDevice(recordingDeviceId, SDL_FALSE);
	//SDL_LockAudioDevice(recordingDeviceId);
}

void RecordAudio::ResetAndStartPlayback(int playbackDeviceId)
{
	bufferBytePosition = 0;
	SDL_PauseAudioDevice(playbackDeviceId, SDL_FALSE);
	//SDL_LockAudioDevice(playbackDeviceId);
}

void RecordAudio::StopRecording(int recordingDeviceId)
{
	SDL_PauseAudioDevice(recordingDeviceId, SDL_TRUE);
	//SDL_UnlockAudioDevice(recordingDeviceId);
}

bool RecordAudio::IsBufferFull()
{
	if (bufferBytePosition > bufferByteMaxPosition)
	{
		return true;
	}

	return false;
}