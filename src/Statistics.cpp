#include "Statistics.h"

Statistics::Statistics()
{
}

void Statistics::Start(Input* p_input, Timer* p_fpsTimer)
{
	totalFrames = 0;
	input = p_input;
	timer = p_fpsTimer;
}

std::string Statistics::ShowCountFrameData(Uint32 p_newFrameCount)
{
	totalFrames += p_newFrameCount;
	double avgFrameTime = 0;
	if (timer->GetTicks() / 1000 > 0)
	{
		avgFrameTime = totalFrames / (timer->GetTicks() / 1000);
	}
	if (avgFrameTime > 2000)
	{
		avgFrameTime = 0;
	}

	fpsCounterText = "FPS: " + std::to_string(avgFrameTime);
	return fpsCounterText;
}

std::string Statistics::ShowInputData()
{
	std::stringstream result;
	result.str("");

	result << "X = " << std::to_string(input->GetMousePosition().x) << " Y = " << std::to_string(input->GetMousePosition().y);
	result << std::endl << "MouseButton Down is " << std::to_string(input->GetMouseButtonDown());
	result << std::endl << "Joystick X = " << std::to_string(input->GetJoystickAxis().x) << " Y = " << std::to_string(input->GetJoystickAxis().y);

	return result.str();
}

Statistics::~Statistics()
{
}