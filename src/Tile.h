#pragma once
#include "AppVideo.h"

const int LEVEL_WIDTH = 1280;
const int LEVEL_HEIGHT = 960;

const int TILE_WIDTH = 80;
const int TILE_HEIGHT = 80;
const int TOTAL_TILES = 192;
const int TOTAL_TILE_SPRITES = 12;

const int TILE_RED = 0;
const int TILE_GREEN = 1;
const int TILE_BLUE = 2;
const int TILE_CENTER = 3;
const int TILE_TOP = 4;
const int TILE_TOPRIGHT = 5;
const int TILE_RIGHT = 6;
const int TILE_BOTTOMRIGHT = 7;
const int TILE_BOTTOM = 8;
const int TILE_BOTTOMLEFT = 9;
const int TILE_LEFT = 10;
const int TILE_TOPLEFT = 11;

class Tile
{
public:
	Tile(int x, int y, int tileType, AppVideo* p_video);
	~Tile();
	void Render(SDL_Rect& p_camera, SDL_Texture* p_texture, SDL_Rect* p_tileClips);
	int GetType();
	SDL_Rect GetBox();

private:
	AppVideo* video;
	SDL_Rect box;
	int type;
};