#include "GameRun.h"
#include "Statistics.h"
#include "DotSprite.h"
#include "RecordAudio.h"
#include "TileManager.h"

#include <iostream>
#include <string>
#include <sstream>
#include <functional>

GameRun::GameRun()
{
}

GameRun::~GameRun()
{
}

int GameRun::Start()
{
	std::cout << "Starting app" << std::endl;

	video = AppVideo();

	if (video.StartVideo() < 0)
	{
		std::cout << "Error starting SDL" << std::endl;
		return 0;
	}

	audio = AppAudio();
	if (audio.StartAudio() < 0)
	{
		std::cout << "Error starting SDL Audio" << std::endl;
		return 0;
	}

	input = Input();
	timer = Timer();
	capFPS = Timer();
	timer.Start();

	return 1;
}

int GameRun::InputTest()
{
	bool quit = false;

	SDL_Event e;
	std::string inputText = "Some text";
	SDL_StartTextInput();

	Statistics statistics = Statistics();
	statistics.Start(&input, &timer);

	while (!quit)
	{
		capFPS.Start();

		bool renderText = false;

		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				if (e.key.keysym.sym == SDLK_BACKSPACE && inputText.length() > 0)
				{
					inputText.pop_back();
					renderText = true;
				}
				else if (e.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL)
				{
					SDL_SetClipboardText(inputText.c_str());
				}
				else if (e.key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL)
				{
					inputText = SDL_GetClipboardText();
					renderText = true;
				}
			}
			else if (e.type == SDL_TEXTINPUT)
			{
				if (!(SDL_GetModState() & KMOD_CTRL
					&& (e.text.text[0] == 'c' || e.text.text[0] == 'C' || e.text.text[0] == 'v' || e.text.text[0] == 'V')))
				{
					SDL_Log(e.text.text);
					inputText += e.text.text;
					renderText = true;
				}
			}
		}

		video.BeginFrame();
		video.DrawText("Enter text input!", 100, 100, 20);
		if (inputText != "")
		{
			video.DrawText(inputText, 100, 200, 20);
		}
		else
		{
			video.DrawText(" ", 100, 200, 20);
		}
		video.EndFrame();

		CapGameLoop(capFPS.GetTicks());
	}

	video.EndVideo();
	SDL_StopTextInput();
	return 1;
}

int GameRun::MovementTest()
{
	bool quit = false;

	DotSprite sprite = DotSprite(0, 0, &input, &video, "res/mm_run.png", 0);
	DotSprite otherSprite = DotSprite(DISPLAY_WIDTH / 4, DISPLAY_HEIGTH / 4, &input, &video, "res/mm_run.png", 1);

	Statistics statistics = Statistics();
	statistics.Start(&input, &timer);

	while (!quit)
	{
		capFPS.Start();

		input.CheckEvents();
		if (input.GetQuitEvent())
		{
			quit = true;
		}

		sprite.HandleInput();
		sprite.MoveVector(otherSprite.GetColliders());
		//otherSprite.Move(sprite.GetColliders());

		video.BeginFrame();
		sprite.Render();
		otherSprite.Render();

		video.DrawText(statistics.ShowInputData(), 50, DISPLAY_HEIGTH - 100, 20);
		video.DrawText(statistics.ShowCountFrameData(1), DISPLAY_WIDTH - 100, DISPLAY_HEIGTH - 50, 20);
		video.EndFrame();

		CapGameLoop(capFPS.GetTicks());
	}

	video.EndVideo();
	return 1;
}

int GameRun::MovementCircleTest()
{
	bool quit = false;

	int levelWidth = 1920;
	int levelHeight = 1080;

	DotSprite sprite = DotSprite(500, 500, &input, &video, "res/mm_run.png", 0);
	DotSprite otherSprite = DotSprite(100, 100, &input, &video, "res/mm_run.png", 1);
	SDL_Texture* background = video.LoadTexture("res/mm_BG.jpg");

	SDL_Rect camera = { 0,0,DISPLAY_WIDTH, DISPLAY_HEIGTH };

	SDL_Rect wall = { 300,40,40,300 };

	Statistics statistics = Statistics();
	statistics.Start(&input, &timer);

	while (!quit)
	{
		capFPS.Start();

		input.CheckEvents();
		if (input.GetQuitEvent())
		{
			quit = true;
		}
		sprite.HandleInput();
		sprite.Move(wall, otherSprite.GetCircleCollider(), levelWidth, levelHeight);
		camera.x = (sprite.GetPosX() + DotSprite::DOTSPRITE_HEIGHT / 2) - (DISPLAY_WIDTH / 2);
		camera.y = (sprite.GetPosY() + DotSprite::DOTSPRITE_WIDTH / 2) - (DISPLAY_HEIGTH / 2);

		if (camera.x < 0)
		{
			camera.x = 0;
		}
		if (camera.y < 0)
		{
			camera.y = 0;
		}
		if (camera.x > levelWidth - camera.w)
		{
			camera.x = levelWidth - camera.w;
		}
		if (camera.y > levelHeight - camera.h)
		{
			camera.y = levelHeight - camera.h;
		}

		int bgX = -camera.x;
		int bgY = -camera.y;

		video.BeginFrame();
		video.DisplayTexture(background, bgX, bgY, levelWidth, levelHeight);
		video.DrawBGRectangle(wall.x - camera.x, wall.y - camera.y, wall.w, wall.h, SDL_Color{ 255,0,0,255 });

		sprite.RenderCircle(camera.x, camera.y);
		otherSprite.RenderCircle(camera.x, camera.y);

		video.DrawText(statistics.ShowInputData(), 50, DISPLAY_HEIGTH - 100, 20);
		video.DrawText(statistics.ShowCountFrameData(1), DISPLAY_WIDTH - 100, DISPLAY_HEIGTH - 50, 20);
		video.EndFrame();

		CapGameLoop(capFPS.GetTicks());
	}

	video.EndVideo();
	return 1;
}

int GameRun::TilesMovement()
{
	bool quit = false;
	SDL_Rect camera = { 0,0,DISPLAY_WIDTH, DISPLAY_HEIGTH };
	DotSprite sprite = DotSprite(500, 500, &input, &video, "res/mm_run.png", 0);
	Tile* tileSet[TOTAL_TILES];
	SDL_Rect tileClips[TOTAL_TILE_SPRITES];
	SDL_Texture* background = video.LoadTexture("res/tiles.jpg");

	if (!TileManager::SetTiles(tileSet, &video, tileClips)) {
		SDL_Log("Failed to load media!\n");
	}

	Statistics statistics = Statistics();
	statistics.Start(&input, &timer);

	while (!quit)
	{
		capFPS.Start();
		input.CheckEvents();
		if (input.GetQuitEvent())
		{
			quit = true;
		}

		sprite.HandleInput();
		sprite.MoveWithTiles(tileSet);

		camera.x = (sprite.GetPosX() + DotSprite::DOTSPRITE_HEIGHT / 2) - (DISPLAY_WIDTH / 2);
		camera.y = (sprite.GetPosY() + DotSprite::DOTSPRITE_WIDTH / 2) - (DISPLAY_HEIGTH / 2);

		if (camera.x < 0)
		{
			camera.x = 0;
		}
		if (camera.y < 0)
		{
			camera.y = 0;
		}
		if (camera.x > LEVEL_WIDTH - camera.w)
		{
			camera.x = LEVEL_WIDTH - camera.w;
		}
		if (camera.y > LEVEL_HEIGHT - camera.h)
		{
			camera.y = LEVEL_HEIGHT - camera.h;
		}

		video.BeginFrame();

		for (int i = 0; i < TOTAL_TILES; ++i)
		{
			tileSet[i]->Render(camera, background, tileClips);
		}
		sprite.Render(camera);

		video.DrawText(statistics.ShowInputData(), 50, DISPLAY_HEIGTH - 100, 20);
		video.DrawText(statistics.ShowCountFrameData(1), DISPLAY_WIDTH - 100, DISPLAY_HEIGTH - 50, 20);
		video.EndFrame();

		CapGameLoop(capFPS.GetTicks());
	}

	return 1;
}

int GameRun::GeneralTest01()
{
	audio.LoadMusic("res/audio/africa-toto.wav");
	audio.LoadMedium("res/audio/guitar.wav");

	bool quit = false;
	SDL_Texture* texture = video.LoadTexture("res/unity.png");
	SDL_Texture* texture2 = video.LoadTexture("res/unreal.png");
	SDL_Texture* mmRunTex = video.LoadTexture("res/mm_run.png");
	std::string displayText = "Nothing";

	std::stringstream timeStream;
	timeStream.str("");

	Statistics statistics = Statistics();
	statistics.Start(&input, &timer);

	SDL_Rect mmRunClips[3];
	mmRunClips[0].x = 0;
	mmRunClips[0].y = 0;
	mmRunClips[0].w = 50;
	mmRunClips[0].h = 50;

	mmRunClips[1].x = 50;
	mmRunClips[1].y = 0;
	mmRunClips[1].w = 50;
	mmRunClips[1].h = 50;

	mmRunClips[2].x = 100;
	mmRunClips[2].y = 0;
	mmRunClips[2].w = 50;
	mmRunClips[2].h = 50;

	int xPos = 0;
	int alpha = 255;
	int gameFrame = 0;
	int animFrame = 3;

	while (!quit)
	{
		capFPS.Start();

		input.CheckEvents();
		if (input.GetQuitEvent())
		{
			quit = true;
		}

		if (input.GetKey(SDL_SCANCODE_UP))
		{
			displayText = "UP";
			alpha++;
		}
		else if (input.GetKey(SDL_SCANCODE_DOWN))
		{
			displayText = "DOWN";
			alpha--;
		}
		else if (input.GetKey(SDL_SCANCODE_LEFT))
		{
			displayText = "LEFT";
			xPos--;
		}
		else if (input.GetKey(SDL_SCANCODE_RIGHT))
		{
			displayText = "RIGHT";
			xPos++;
			input.RumbleJoystick(0.5, 500);
		}
		else if (input.GetKey(SDL_SCANCODE_P))
		{
			displayText = "MUSIC";
			audio.PlayMusic();
		}
		else if (input.GetKey(SDL_SCANCODE_O))
		{
			displayText = "AUDIO";
			audio.PlayMedium();
		}
		else if (input.GetKey(SDL_SCANCODE_1))
		{
			timer.Pause();
		}
		else if (input.GetKey(SDL_SCANCODE_2))
		{
			timer.Unpause();
		}

		video.BeginFrame();

		if (xPos > DISPLAY_WIDTH)
		{
			xPos = 0;
		}
		if (xPos < 0)
		{
			xPos = DISPLAY_WIDTH;
		}
		if (alpha > 255)
		{
			alpha = 0;
		}
		if (alpha < 0)
		{
			alpha = 255;
		}

		SDL_Color color = SDL_Color{ 0, 255, 0, 255 };
		SDL_Color color2 = SDL_Color{ 0, 0, 255, 255 };
		video.DrawBGRectangle(0, 0, 100, 100, color);
		video.DrawBGRectangle(xPos, 100, 100, 100, color2);
		video.DisplayTexture(texture, xPos, 100, 100, 100);
		video.DisplayTextureBlend(texture2, alpha, xPos, 100, 100, 100);
		SDL_Rect clip;
		clip.h = 256;
		clip.w = 256;
		clip.x = 0;
		clip.y = 0;
		video.DisplayTextureClip(texture, &clip, 200, 200, 100, 100);

		double joystickAngle = atan2(double(input.GetJoystickAxis().y), double(input.GetJoystickAxis().x)) * (180.0 / M_PI);
		if (input.GetJoystickAxis().y == 0 && input.GetJoystickAxis().x == 0)
		{
			joystickAngle = 0;
		}
		video.DisplayTextureClipEx(texture2, NULL, 300, 200, 100, 100, joystickAngle, NULL, SDL_FLIP_VERTICAL);

		//Megaman animation
		gameFrame++;
		if (gameFrame / animFrame > animFrame - 1)
		{
			gameFrame = 0;
		}
		int clipNumber = gameFrame / animFrame;
		video.DisplayTextureClip(mmRunTex, &mmRunClips[clipNumber], 300, 300, 100, 100);

		video.DrawText(displayText, 200, 200, 20);
		video.DrawText(statistics.ShowInputData(), 50, DISPLAY_HEIGTH - 100, 20);

		timeStream.str("");
		timeStream << "Time since Start is " << (timer.GetTicks()) / 1000;
		video.DrawText(timeStream.str().c_str(), 300, DISPLAY_HEIGTH - 95, 20);

		video.DrawText(statistics.ShowCountFrameData(1), DISPLAY_WIDTH - 100, DISPLAY_HEIGTH - 50, 20);

		video.EndFrame();

		CapGameLoop(capFPS.GetTicks());
	}

	video.EndVideo();
	return 1;
}

int GameRun::SoundRecordTest()
{
	bool quit = false;

	std::string infoText = "Select the audio device";
	std::string selectedDevice = "";
	RecordAudio record = RecordAudio();
	record.loadMedia();
	for (int i = 0; i < record.DevicesNames()->size(); i++)
	{
		std::cout << record.DevicesNames()->at(i) << std::endl;
	}

	RecordingState state = SELECTING_DEVICE;
	SDL_AudioDeviceID recordingDeviceId = 0;
	SDL_AudioDeviceID playbackDeviceId = 0;

	while (!quit)
	{
		capFPS.Start();
		input.CheckEvents();

		if (input.GetQuitEvent())
		{
			quit = true;
		}

		video.BeginFrame();
		int deviceID = -1;

		switch (state)
		{
		case SELECTING_DEVICE:
			if (input.GetKey(SDL_SCANCODE_1))
			{
				deviceID = 0;
			}
			else if (input.GetKey(SDL_SCANCODE_2))
			{
				deviceID = 1;
			}
			else if (input.GetKey(SDL_SCANCODE_3))
			{
				deviceID = 2;
			}
			else if (input.GetKey(SDL_SCANCODE_4))
			{
				deviceID = 3;
			}

			if (deviceID >= 0 && deviceID < record.DevicesNames()->size())
			{
				selectedDevice = record.DevicesNames()->at(deviceID);
			}

			video.BeginFrame();
			video.DrawText(infoText, 200, 150, 20);
			for (int i = 0; i < record.DevicesNames()->size(); i++)
			{
				std::string deviceName = std::to_string(i + 1) + ") " + record.DevicesNames()->at(i);
				video.DrawText(deviceName, 200, 200 + (25 * i), 20);
			}
			video.EndFrame();

			if (deviceID != -1)
			{
				state = SELECTED;
			}

			break;
		case SELECTED:
			infoText = "Opening Device";
			recordingDeviceId = record.OpenRecordingDevice(deviceID);

			if (recordingDeviceId == 0)
			{
				std::cout << "Failed to open audio recording device" << std::endl;
				state = ERROR;
			}
			else
			{
				std::cout << "audio recording device" << std::endl;
				playbackDeviceId = record.OpenPlaybackDevice();

				if (playbackDeviceId == 0)
				{
					std::cout << "Failed to open audio playback device" << std::endl;
					state = ERROR;
				}
				else
				{
					std::cout << "Playback and recording opened. Setting up" << std::endl;
					record.SetupRecording();
				}
			}

			state = STOPPED;
			video.DrawText(infoText, 200, 200, 20);

			break;
		case STOPPED:
			infoText = "Current Device:";

			if (input.GetKey(SDL_SCANCODE_1))
			{
				record.ResetAndStartRecording(recordingDeviceId);
				state = RECORDING;
			}

			video.DrawText(infoText, 200, 150, 20);
			video.DrawText(selectedDevice, 200, 200, 20);
			video.DrawText("Press 1 to start recording", 200, 250, 20);

			break;
		case RECORDING:
			infoText = "RECORDING...";

			SDL_LockAudioDevice(recordingDeviceId);
			if (record.IsBufferFull())
			{
				record.StopRecording(recordingDeviceId);
				infoText = "RecordDone";
				state = RECORDED;
			}
			SDL_UnlockAudioDevice(recordingDeviceId);

			video.DrawText(infoText, 200, 200, 20);
			break;
		case RECORDED:
			infoText = "Press 1 to start Playback:";

			if (input.GetKey(SDL_SCANCODE_1))
			{
				record.ResetAndStartPlayback(playbackDeviceId);
				state = PLAYBACK;
			}

			video.DrawText(infoText, 200, 200, 20);
			break;
		case PLAYBACK:

			SDL_LockAudioDevice(playbackDeviceId);
			if (record.IsBufferFull())
			{
				SDL_PauseAudioDevice(playbackDeviceId, SDL_TRUE);
				state = RECORDED;
			}
			SDL_UnlockAudioDevice(playbackDeviceId);
			video.DrawText("PLAYING", 200, 200, 20);
			break;
		}

		video.EndFrame();

		CapGameLoop(capFPS.GetTicks());
	}

	return 1;
}

void GameRun::CapGameLoop(Uint32 p_currentFrameTicks)
{
	if (p_currentFrameTicks < SCREEN_TICKS_PER_FRAME)
	{
		SDL_Delay(SCREEN_TICKS_PER_FRAME - p_currentFrameTicks);
	}
}