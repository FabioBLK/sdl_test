#include <sstream>
#include "Input.h"
#include "Timer.h"

class Statistics
{
public:
	Statistics();
	~Statistics();
	void Start(Input* p_input, Timer* p_fpsTimer);
	std::string ShowCountFrameData(Uint32 p_newFrameCount);
	std::string ShowInputData();

private:
	std::string mousePositionText;
	std::string joystickAxis01Text;
	std::string fpsCounterText;
	Uint32 totalFrames = 0;
	Timer* timer = nullptr;
	Input* input = nullptr;
};